## TACT - plateforme de Transcription et d'Annotation de Corpus Textuels

TACT est un logiciel développé au sein de l'[UMR Litt&Arts](https://litt-arts.univ-grenoble-alpes.fr/) (Université Grenoble Alpes, CNRS) qui s'appuie principalement sur [Symfony 4](https://symfony.com/4). Il offre notamment la possibilité d'héberger différents projets de transcription et d’annotation de sources numérisées, chacun pouvant avoir ses spécificités (communauté plus ou moins ouverte, simple transcription ou annotation selon un schéma, nombre de relectures nécessaires, etc.). Cela permet de déléguer la transcription à des non-spécialistes, tout en contrôlant la qualité de l’exécution, et ce via une interface simple d’utilisation.

### Documentation
Des manuels sont disponibles sur la plateforme :
  * [Manuel contributeur](https://tact.demarre-shs.fr/platform/manuel_contributeur.pdf)
  * [Manuel gestionnaire](https://tact.demarre-shs.fr/platform/manuel_gestionnaire.pdf)

Une ébauche de documentation est disponible sur le
[wiki](https://gitlab.com/litt-arts-num/tact/-/wikis/home)

### Contributions
voir le [Gitlab graph](https://gitlab.com/litt-arts-num/tact/-/graphs/master) et [le fichier AUTHORS](AUTHORS.md)

### Merci
* Julien Fagot (CDD UMR Litt&Arts) pour son [application](https://gitlab.com/litt-arts-num/tact-related/tact-tools) de création et gestion de schéma
* Myriam EL HELOU & Sami BOUHOUCHE (Master 2 IDL UGA) pour leur [script Python](https://github.com/elheloum/TEI2JSON) de création de schema. Une version maintenue est disponible [ici](https://gitlab.com/litt-arts-num/tei2json).

(Manifestez vous si on vous a oublié...)

### Licence
GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))
