<?php

namespace App\Service;

use App\Entity\Review;
use App\Entity\ReviewRequest;
use App\Entity\Transcription;
use App\Entity\User;
use App\Service\FlashManager;
use App\Service\MailManager;
use App\Service\MessageManager;
use App\Service\TranscriptionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Translation\TranslatorInterface;

class ReviewRequestManager
{
    protected $em;
    protected $fm;
    protected $messageManager;
    protected $mm;
    protected $router;
    protected $security;
    protected $tm;
    protected $translator;

    public function __construct(
        EntityManagerInterface $em,
        FlashManager $fm,
        MailManager $mm,
        Security $security,
        TranscriptionManager $tm,
        MessageManager $messageManager,
        TranslatorInterface $translator,
        UrlGeneratorInterface $router
    ) {
        $this->em = $em;
        $this->mm = $mm;
        $this->fm = $fm;
        $this->tm = $tm;
        $this->messageManager = $messageManager;
        $this->security = $security;
        $this->translator = $translator;
        $this->router = $router;
    }

    public function create(Transcription $transcription, User $user = null)
    {
        $user = (!$user) ? $this->security->getUser() : $user;
        if (!$request = $transcription->getReviewRequest()) {
            $media = $transcription->getMedia();
            $project = $media->getProject();

            // Create request
            $request = new ReviewRequest();
            $request->setUser($user);
            $request->setTranscription($transcription);
            $this->em->persist($request);

            // Create log
            $log = $this->tm->addLog($transcription, AppEnums::TRANSCRIPTION_LOG_WAITING_FOR_VALIDATION, null, false, $user);
            $this->em->persist($log);

            // Suppression du dernier lock log pour libérer la transcription
            $lockLog = $this->tm->getLastLockLog($transcription);
            $locked = $lockLog ? $this->tm->isLocked($lockLog) : false;
            if ($locked && $this->tm->isLockedByCurrentUser($transcription, $lockLog)) {
                $this->em->remove($lockLog);
            }

            // send mail to subscribed users
            $this->mm->sendReviewRequest($project, $transcription);

            // send intern messages to reviewers and managers
            $users = $this->em->getRepository(User::class)->getReviewersByProject($project);
            $url = $this->router->generate("media_transcription_review", ["id" => $media->getId()]);
            $msg = $this->translator->trans("new_review_request_with_media", ['%url%' => $url, '%mediaName%' => $media->getName(), '%projectName%' => $project->getName()]);
            $this->messageManager->create($users, $msg, null, false);

            // flush
            $this->em->flush();

            $this->fm->add('notice', 'review_request_created');
        } else {
            $this->fm->add('notice', 'already_pending_review');
        }

        return $request;
    }

    public function delete(ReviewRequest $request)
    {
        $this->em->remove($request);
        $this->em->flush();

        return;
    }
}
