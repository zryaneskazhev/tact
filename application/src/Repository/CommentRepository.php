<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findByProject(Project $project)
    {
        $projectId = $project->getId();

        return $this->createQueryBuilder('c')
        ->select('c')
        ->leftJoin('c.transcription', 't')
        ->leftJoin('t.media', 'm')
        ->leftJoin('m.project', 'p')
        ->andWhere('p.id = :pId')
        ->addOrderBy('c.createdAt', 'DESC')
        ->addOrderBy('p.id', 'DESC')
        ->setParameter('pId', $projectId)
        ->getQuery()
        ->getResult();
    }
    // public function findUnarchivedSortedByLog()
    // {
    //     return $this->createQueryBuilder('p')
    //       ->select('p')
    //       ->leftJoin('p.medias', 'm')
    //       ->leftJoin('m.transcription', 't')
    //       ->leftJoin('t.transcriptionLogs', 'l')
    //       ->andWhere('p.archived = 0')
    //       ->addOrderBy('l.createdAt', 'DESC')
    //       ->addOrderBy('p.id', 'DESC')
    //       ->getQuery()
    //       ->getResult();
    // }
}
