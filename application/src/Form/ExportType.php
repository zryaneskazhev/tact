<?php

namespace App\Form;

use App\Entity\Directory;
use App\Service\DirectoryManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExportType extends AbstractType
{
    protected $dm;

    public function __construct(DirectoryManager $dm)
    {
        $this->dm = $dm;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('medias', CheckboxType::class, [
                'label' => 'export_medias',
                'required' => false
            ])
            ->add('dir', EntityType::class, [
              'class' => Directory::class,
              'label' => 'export_root_directory',
              'help' => "export_root_directory_help",
              'choice_label' => function($dir) {
                return $this->dm->getFullPath($dir);
              },
              'placeholder' => 'Racine',
              'required' => false,
              'query_builder' => function (EntityRepository $er) use ($options) {
                  return $er->createQueryBuilder('d')
                            ->andWhere('d.project = '.$options["project"]->getId());
              },
            ])
            ->add('transcription_status', ChoiceType::class,[
              'label' => 'export_transcription_status',
              'help' => 'export_transcription_status_help',
              'translation_domain' => 'messages',
              'choice_translation_domain' => 'messages',
              'required' => false,
              'placeholder' => 'Toutes',
              'choices'  => [
                'export_valid_only' => 'valid_only'
              ],
            ])
            ->add('transcriptions', CheckboxType::class, [
                'label' => 'export_transcriptions',
                'required' => false
            ])
            ->add('transcriptions_metadatas', CheckboxType::class, [
                'label' => 'export_transcriptions_metadatas',
                'required' => false,
                'help' => "export_transcriptions_metadatas_help"
            ])
            ->add('transcriptions_apply_xsl', CheckboxType::class, [
                'label' => 'export_transcriptions_apply_xsl',
                'required' => false,
                'help' => "export_transcriptions_apply_xsl_help"
            ])
            ->add('transcriptions_list', CheckboxType::class, [
                'label' => 'export_transcriptions_list',
                'required' => false
            ])
            ->add('users_list', CheckboxType::class, [
                'label' => 'export_users_list',
                'required' => false
            ])
            ->add('project_infos', CheckboxType::class, [
                'label' => 'export_project_infos',
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'export_launch',
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'project' => null
        ));
    }
}
